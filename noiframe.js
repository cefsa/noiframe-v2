function NoIframe(elementId, url, assets){
    this.assets = assets;
    this.urlToLoad = url;
    this.elementId = this.cleanID(elementId);
    
    this.addCss();
    this.loadHtml();
}

NoIframe.prototype = {
    cleanID: function(elementId) {
         // Replace `#` char in case the function gets called `querySelector` or jQuery style
        const elemId = elementId;
        if (elementId.startsWith("#")) {
            elemId = elementId.replace("#", "");
        }
        return elemId;
    },
    addCss: function(){
        for(let i = 0; i < this.assets.css.length; i++){
            let d=document,
            h=d.getElementsByTagName('head')[0],
            l=d.createElement('link');
            l.rel='stylesheet';
            l.type='text/css';
            l.async=true;
            l.href=this.assets.css[i];
            h.appendChild(l);
        }
    },
    addJs: function(){
        for(let i = 0; i < this.assets.js.length; i++){
            let d2=document,
            h2=d2.getElementsByTagName('head')[0],
            s2=d2.createElement('script');
            s2.type='text/javascript';
            s2.async=true;
            s2.src=this.assets.js[i];
            h2.appendChild(s2);
        }
    },
    loadHtml: function(){
        const init = {
            method : "GET",
            headers : { "Content-Type" : "text/html" },
            mode : "cors",
            cache : "default"
        };
        const req = new Request(this.urlToLoad, init);
        const that = this;
        fetch(req)
            .then(function(response) {
                return response.text();
            })
            .then(function(body) {
                document.getElementById(that.elementId).innerHTML = body;
                that.addJs();
            });
    }
}