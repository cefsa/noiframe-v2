NoIframe
----------------------------------------------------------

####NoIframe é uma iniciativa alternativa para o uso do **Iframe**.


##Instruções de uso:

###Inclusão do script do plugin na página.

Deve ser incluído o script do plugin na página. 

ex: 

              <script src="https://infogbucket-dev.s3.amazonaws.com/noiframe/noiframe.js"></script>

###Inclusão do elemento container           
        
Deve ser incluíndo na página um elemento para servir de container. Esse elemento deverá conter os atributos abaixo:

- **id:** Deve criado um identificador único para cada elemento.
- **class:** Deve ser incluído a classe "noiframe-container"

ex:
     
              <div id="infogcalc" class="noiframe-container"></div>

###Inicialização do plugin.

Para inicializar o plugin devemos alterar os valores das constantes abaixo:

- **infogUrl:** Deve ser preenchido com a URL da página a ser incorporada.
- **infogAssets:** Este parametro possui 2 tipos de arquivos (CSS, JS).
       - **CSS:** Deve ser incluído as URL's dos arquivos de CSS.
       - **JS:** Deve ser incluído as URL's dos arquivos de JS.

**Parâmetros para a inicialização do plugin:**

- Devemos passar o **ID** do elemento container como **primeiro** parâmetro.
- Devemos passar o **infoUrl** como **segundo** parâmetro.
- Devemos passar o **infogAssets** como **terceiro** parâmetro.

ex:

                <script>
                        (function(){
                                const infogUrl = "https://infograficos-estaticos.s3.amazonaws.com/calculadoras-previdencia-2019/index.html";
                                const infogAssets = {
                                    "css": ["https://infograficos-estaticos.s3.amazonaws.com/calculadoras-previdencia-2019/css/main.css"],
                                    "js": ["https://infograficos-estaticos.s3.amazonaws.com/calculadoras-previdencia-2019/js/bundle.js"]
                                };
                                const noIframe = new NoIframe('infogcalc', infogUrl, infogAssets);
                        })();
                </script>